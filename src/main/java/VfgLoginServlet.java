import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class VfgLoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		if (request.getRequestURI().endsWith("login/auth")) {
			authenticateAndRedirect(request, response);
		} else {
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);

			String htmlFile = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("index.html"));
			htmlFile = StringUtils.replace(htmlFile, "*returnUrl*", request.getParameter("returnUrl"));
			htmlFile = StringUtils.replace(htmlFile, "*opco*", request.getParameter("opco"));
			htmlFile = StringUtils.replace(htmlFile, "*channel*", request.getParameter("channel"));

			response.getWriter().write(htmlFile);
		}
	}

	private void authenticateAndRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean retry = true;
		String token = null;
		do {
			String msisdn = request.getParameter("msisdn");
			String url = "http://dgig-dit-dit1.sp.vodafone.com:30645/sso/opensso/identity/authenticate?username=gig&uri=module=MSISDN&password=" + msisdn;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestProperty("Authorization", "Basic VkZURVNUOlZGVEVTVA==");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 401) {
				String opco = request.getParameter("opco");
				String channel = request.getParameter("channel");
				createIndividual(msisdn, opco, channel);
			} else {
				token = getTokenFromResponse(getResponseAsString(con, false));
				retry = false;
			}
		} while (retry == true);

		Cookie cookie = new Cookie("vfgssotoken", URLEncoder.encode(token, "UTF-8"));
		cookie.setDomain(".vodafone.com");
		cookie.setPath("/");
		cookie.setMaxAge(-1);

		response.addCookie(cookie);

		String returnUrl = request.getParameter("returnUrl");
		response.sendRedirect(returnUrl);
	}

	private String getTokenFromResponse(String response) throws IOException {
		return StringUtils.substringAfter(response, "token.id=");
	}

	private String getResponseAsString(HttpURLConnection con, boolean error) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(error ? con.getErrorStream() : con.getInputStream()));
		String inputLine;
		StringBuffer res = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			res.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(res.toString());
		return res.toString();
	}

	private void createIndividual(String msisdn, String opco,String channel) throws IOException {
		String url = "http://dgig-dit-dit1.sp.vodafone.com:30830/lifecycle/individual";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/xml; charset=UTF-8");
		con.setRequestProperty("Authorization", "Basic VkZURVNUOlZGVEVTVA==");
		con.setRequestProperty("vf-gig-ci-type", "MSISDN");
		con.setRequestProperty("vf-gig-ci-identifier", "00000");
		con.setRequestProperty("vf-gig-ti-application-id", "VFG Login");
		con.setRequestProperty("vf-gig-ti-application-id", "PPE");
		con.setRequestProperty("vf-gig-ti-service-id", "registration");
		con.setRequestProperty("vf-gig-ti-service-version", "1.0.0");
		con.setRequestProperty("vf-gig-ti-timestamp", "2016-02-10T17:36:41Z");
		con.setRequestProperty("vf-gig-ti-reference-id", "814537748");
		con.setRequestProperty("vf-gig-ti-identity-id", "127.0.0.1");
		con.setRequestProperty("x-vf-audit-partner-id", "LIFECYCLE LOGIN APP");

		con.setDoOutput(true);

		String payload = String.format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns3:individual xmlns:ns2=\"http://www.vodafone.com/vf/core_common/types/v0_4\" xmlns:ns3=\"http://www.vodafone.com/vf/gig/lifecycle/types/v1_0\">"
				+ "<services><service><name>PARTNEROFFERS</name></service></services>"
				+ "<credentials><credential><username>%s@vf.com</username><password>Abcd1234</password></credential></credentials>"
				+ "<msisdns><msisdn><msisdn>%s</msisdn>"
				+ "<attributes><attribute><ns2:name>country</ns2:name><ns2:value>%s</ns2:value></attribute>"
				+ "<attribute><ns2:name>verified</ns2:name><ns2:value>true</ns2:value></attribute>"
				+ "<attribute><ns2:name>status</ns2:name><ns2:value>true</ns2:value></attribute></attributes></msisdn></msisdns>"
				+ "<attributes><attribute><ns2:name>channel</ns2:name><ns2:value>%s</ns2:value></attribute>"
//				+ "<attribute><ns2:name>accountType</ns2:name><ns2:value>PROSPECT</ns2:value></attribute>"
				+ "</attributes></ns3:individual>", msisdn, msisdn, opco.toUpperCase(), channel);

		OutputStream os = con.getOutputStream();
		os.write(payload.getBytes("UTF-8"));
		os.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		getResponseAsString(con, responseCode != 200 && responseCode != 201);
	}
}